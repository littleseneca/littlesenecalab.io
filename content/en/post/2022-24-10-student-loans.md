---
title: "Why I won’t accept a student loan handout"
date: 2022-10-24T12:45:39-06:00
draft: false
tags: ["scene"]
---
# A Disclaimer
I want to start this post with a disclaimer. I think there are lots of people who could benefit from debt relief. For example, anyone who is permanently disabled and has no ability to pay down their student loans should not have to worry about the state of their debt (And there is currently a program for this exact situation). This post is not in any way intended to be a post against student loan reform. That post will come later. This post is aimed ONLY at the newly announced general debt forgiveness plan. 

With all that said, I think that the Student Aid package is a disturbingly blatant attempt to buy votes using my tax dollars right before a major election, and a completely ineffectual tool for the purpose of relieving student loan debt. 

# Addressing Bribery
>“A democracy is always temporary in nature; it simply cannot exist as a permanent form of government.  A democracy will continue to exist up until the time that voters discover they can vote themselves generous gifts from the public treasury.  From that moment on, the majority always votes for the candidates who promise the most benefits from the public treasury, with the result that every democracy will finally collapse due to loose fiscal policy, which is always followed by a dictatorship.” 

Alexander Tyler – 18th Century Scholar

# Addressing Fiscal Irresponsibility 
I participate in an online forum for student loan borrowers. I get to see the sentiment from many of my peers who are still in deep student loan debt Many of them have far in excess of $20,000 in active loans. Prudence would say that these individuals should simply continue paying down their loans, and when the debt reduction comes, enjoy a 10-20K reduction in their loan balance. 

That would be the smart response. But, what I’ve seen is people who are pre-maturely withdrawing their refunds, increasing their student loan balance. Some of these people are putting their refunds into high interest savings account, which isn’t the worst idea. But many others are just spending the money. 

So, rather than the intended goal of reducing the burden of student loans, it appears that this student loan forgiveness plan has simply provided a massive stimulus check to an already comparatively successful portion of the nation. Furthermore, the forgiveness announced by this administration does nothing to reduce the preexisting conditions which created our current nation’s $1.75 trillion dollars in student loan debt. Finally, this bill continues the trend of encouraging the US economy to spend money it does not have. Those are the facts. Below is the nuance.

# Addressing Fairness
My wife and I already paid off over $60,000 in student loans and are debt free. We did this hard work exclusively on our own, without any support, and without asking for help. We sacrificed or early twenties and made hard life choices. As our friends were buying cars and houses, we were pushing all our available funds towards our debt bills. While they were building debt, we became debt free. We sacrificed while they ignored their responsibilities. And now, we are debt free and they are swimming in bills. In a normal society, this would be a fair trade. We could look back on our choices and feel happy, knowing that we made the right call. In a normal society, we would be looked at as wise financial planners. 

Now, a brief note, we aren’t the kind of people who tell others how to live their lives or belittle people who don’t live like us. That’s a great way to not have friends. But, we do observe and take notes. Interestingly, when the subject of finances and  student loans comes up with friends we really only hear from two camps. Either we get congratulated for our financial fortitude or we get ridiculed. And I’ve gotten used to that attitude of ridicule. It exists wherever mediocrity exists. Ridicule followed me in college as I studied hard and others partied. Ridicule followed me as I worked extra hours while other clocked out early. Ridicule is simply a byproduct of success. 

And now, the mediocrity of low expectations is being rewarded with free money. And non of the  lethal pre-existing conditions of interest backed student loans are being addressed. This won’t help reduce student loan interest rates. This won’t help reduce the cost of tuition. This won’t help increase funding for public institutions. All it does is incentives bad behavior for the majority of loan borrowers while providing relief for a minority (Click here to see the statistics from investopedia.com) of a minority of the American public. Remember, most Americans do not have advanced degrees, and all student loans are voluntarily initiated. 

# Addressing Social Decay
All of this would be fine, in a healthy society. People fail to pay back loans all the time. They make bad personal choices. That is a byproduct of society. But here is the problem. The mediocrity of the underachieving has been rewarded with free money. This is antithetical to the principles of hard work and proved value. In other words, we are incentivizing bad behavior. 

I always thought we were supposed to look up to people who worked hard and lived independent lives. People who made their own way. This is the very core of the American Dream, is it not? Immigrants coming from all over the world, who don’t speak a lick of English,  pushing themselves to the very edge of their ability in a foreign land, proving themselves in spite of adversity (and blatant racism), forging a future for themselves and their offspring. That is the American dream. That anyone can be successful with the right kind of hard work and creativity. That dream is beautiful. And I want to see that dream become a reality for all who want it. 

But, that is not the dream anymore. That is not what we are being pitched. The new dream is an easy life. A world where effort is no longer required to produce prosperity. A world where we can simply redistribute wealth to produce equality and design our own universe. 

And frankly, that dream can work. For a while. But the dream of decadence always ends with decay. Every highly successful nation becomes swallowed by its own avarice and greed. At the end of the day, survival of the fittest always wins out. The strong devour the weak and decadence always, always produces death. The Visigoths always sack Rome. 

# My Personal Response
I do not believe in free things. Everything in life has its price. I could accept this “free” money from the federal government. I could instantly become twenty thousand dollars richer. I could finally begin the journey of home ownership with this money. And I am sorely tempted to say yes. But to do so would be to accept this modern dream of America. And I simply refuse. Everything has its price. And for me, the price of this money is the statement that I agree with this new American dream, and I think that the old American dream is dead. 

This is a watershed moment for me. I’ve never felt this strong a conviction before. Because it was never personal before. It was always theoretical. But now I am being offered twenty thousand dollars. And all I have to do is accept the money. 

But I won’t. Because I am not weak. I do not require a handout, nor do I want a handout. Save it for the people who actually need it. Save it for the people who have no other options. Save it for the people who actually need a social safety net. As for me, I will do just fine on my own. I don’t need the government to help me build my American dream. 

