---
title: "Articles"
date: 2017-03-02T12:00:00-05:00
---
Below are some of the musings that I have chosen to submit for your review. They range in content from simple how to guides for linux server management, to treatises on on taxation and privacy.
